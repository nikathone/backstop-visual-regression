# Visual Regression Testing using [BackstopJS](https://github.com/garris/BackstopJS/)

This repository was started from https://github.com/davidneedham/visual-regression-testing-workshop but it has been updated to allow the usage of a local `config.yml`.

[BackstopJS](https://github.com/garris/BackstopJS/) is used for the visual regression testing. The app itself is built with [Node JS](https://nodejs.org/), [`commander.js`](https://github.com/tj/commander.js/), and [`Inquirer.js`](https://github.com/SBoudrias/Inquirer.js).

## Prerequisites

You will need:

- A local development environment with [Node JS/NPM](https://docs.npmjs.com/getting-started/installing-node)
- [Google Chrome](https://www.google.com/chrome/)
- A live, web-accessible website
- Another environment of the website above (e.g. local, staging, etc.)

### Getting The Code

Create a new repository from this template and then either using Git clone or download the `.zip` file of your copy.

## Instructions

After setting up the repository locally (see above) you will need to:

1. Run the command `npm install` to download dependencies
   - This only needs to be done once
1. Run the command `npm run start`
   - Select the site you want to test from the list
   - Note: `npm run start` can be used anytime you want to run the app
1. Check out the results from the sample test
   - They should open in your browser automatically
1. Copy `example.config.yml` to `config.yml `and update it accordingly.
   - This is where the list of sites to test is stored
   - Try changing to one (or more) of your sites
   - `nonProductionBaseUrl` is your non-production environment (local, staging, etc.) URL
   - `productionBaseUrl` is your production site URL
   - Adjust `pathsToTest`, which is the array of URIs to test for each site
1. There are other configurations in the `config.yml` to adjust viewports, delay, hidden selectors, etc.
1. Run the command `npm run start`.
   - Select the site you want to test from the list
   - Or run `npm run start -- --site=<site-machine-name>` to skip the option to choose a site.

## Troubleshooting

If you are having issues with the script hanging or BackstopJS taking a long time there may be headless Chrome instances that didn't close properly.

1. Try `pkill -f "(chrome)?(--headless)"` on Mac/Linux or `Get-CimInstance Win32_Process -Filter "Name = 'chrome.exe' AND CommandLine LIKE '%--headless%'" | %{Stop-Process $_.ProcessId}` in Windows PowerShell.
1. Or delete the `backstop_data` folder and re-run the script again.
