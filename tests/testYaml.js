const yaml = require('js-yaml');
const fs = require('fs');

try {
  const config = yaml.load(fs.readFileSync('config.yml', 'utf8'), {json: true});
  console.log(config);
}
catch (e) {
  console.error('No sitesToTest.yml file was found! Please copy example.sitesToTest.yml to sitesToTest.yml and update it to match your sites configurations.');
}
