const yaml = require('js-yaml');
const fs = require('fs');

const untrailingSlashIt = require('./utils').untrailingSlashIt;
const trailingSlashIt = require('./utils').trailingSlashIt;
const leadingSlashIt = require('./utils').leadingSlashIt;

const loadConfig = function () {
    const defaultConfig = {
        'delayTime': 1500,
        'acceptableThreshold': 0.1,
        'debugWindow': false,
        'debug': false,
        'viewports': [{
                'name': 'phone',
                'width': 320,
                'height': 480,
            },
            {
                'name': 'desktop',
                'width': 1920,
                'height': 1080,
            },
        ],
        asyncCaptureLimit: 10,
    };
    let localConfig = {};
    try {
        localConfig = yaml.load(fs.readFileSync('config.yml', 'utf8'), {
            json: true
        });
    } catch (e) {
        console.error('No config.yml file was found! Please copy example.config.yml to config.yml and update it to match your test configurations.');
    }

    return {
        ...defaultConfig,
        ...localConfig
    };
};

module.exports = function backstopConfig(nonProductionBaseUrl, productionBaseUrl, pathsToTest, siteName) {
    const configData = loadConfig();
    const backstopDataDir = `backstop_data/${siteName}`;
    const delayTime = configData.delayTime;
    const acceptableThreshold = configData.acceptableThreshold;

    const config = {
        'id': siteName,
        asyncCaptureLimit: configData.asyncCaptureLimit,
        'viewports': configData.viewports,
        'scenarios': [{
            'label': 'Homepage',
            'url': trailingSlashIt(nonProductionBaseUrl),
            'referenceUrl': trailingSlashIt(productionBaseUrl),
            'hideSelectors': [],
            'selectors': ['document'],
            'readyEvent': null,
            'delay': delayTime,
            'misMatchThreshold': acceptableThreshold
        }],
        'paths': {
            'ci_report': `${backstopDataDir}/ci_report`,
            'json_report': `${backstopDataDir}/json_report`,
            'html_report': `${backstopDataDir}/html_report`,
            'bitmaps_reference': `${backstopDataDir}/bitmaps_reference`,
            'bitmaps_test': `${backstopDataDir}/bitmaps_test`,
            'compare_data': `${backstopDataDir}/bitmaps_test/compare.json`,
            'casper_scripts': `${backstopDataDir}/casper_scripts`,
            'engine_scripts': `${backstopDataDir}/engine_scripts`
        },
        'engine': 'puppeteer',
        'report': ['browser', 'json'],
        'casperFlags': [],
        'port': 3001,
        debugWindow: configData.debugWindow,
        debug: configData.debug,
    };

    const scenarios = pathsToTest.map(function (path) {
        return {
            'label': path,
            'url': untrailingSlashIt(nonProductionBaseUrl) + leadingSlashIt(path),
            'referenceUrl': untrailingSlashIt(productionBaseUrl) + leadingSlashIt(path),
            'hideSelectors': [],
            'selectors': ['document'],
            'readyEvent': null,
            'delay': delayTime,
            'misMatchThreshold': acceptableThreshold
        };
    });

    config.scenarios = config.scenarios.concat(scenarios);
    return config;
};