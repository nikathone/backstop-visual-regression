const yaml = require('js-yaml');
const fs = require('fs');

let config = {};

try {
  config= yaml.load(fs.readFileSync('config.yml', 'utf8'), {json: true});
}
catch (e) {
  console.error('No config.yml file was found! Please copy example.config.yml to config.yml and update it to match your test configurations.');
}

module.exports = config.hasOwnProperty('sites') ? config.sites : {};
